SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `entries`;
CREATE TABLE `entries` (
  `text` varchar(100) COLLATE utf8_polish_ci NOT NULL DEFAULT '',
  `reply` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
